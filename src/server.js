const express = require('express')
const nunjucks = require('nunjucks')
const path = require('path')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const flash = require('connect-flash')
const dateFilter = require('nunjucks-date-filter')

var http = require('http')
var socketIo = require('socket.io')

class App {
  constructor () {
    this.express = express()
    this.isDev = process.env.NODE_DEV !== 'production'

    this.middlewares()
    this.socket()
    this.views()
    this.routes()
  }

  socket () {
    this.server = http.Server(this.express)
    const io = socketIo(this.server)

    this.express.use((req, res, next) => {
      req.io = io
      return next()
    })
  }

  middlewares () {
    this.express.use(express.urlencoded({ extended: false }))
    this.express.use(express.json())
    this.express.use(flash())
    this.express.use(
      session({
        name: 'senacsesel',
        secret: 'aPPg0$enaC',
        resave: true,
        store: new FileStore({
          path: path.resolve(__dirname, '..', 'tmp', 'sessions')
        }),
        saveUninitialized: true
      })
    )
  }

  views () {
    const env = nunjucks.configure(path.resolve(__dirname, 'app', 'views'), {
      autoescape: true,
      express: this.express,
      watch: this.isDev
    })

    env.addFilter('date', dateFilter)

    this.express.set('view engine', 'njk')
    this.express.use(express.static(path.resolve(__dirname, 'public')))
  }

  routes () {
    this.express.use(require('./routes'))
  }
}

module.exports = new App().server
