const express = require('express')
const multerConfig = require('./config/multer')
const upload = require('multer')(multerConfig)
const numeral = require('numeral')
const moment = require('moment')

const routes = express.Router()

const authMiddlware = require('./app/middlewares/auth')
const guestMiddlware = require('./app/middlewares/guest')
const providerMiddleware = require('./app/middlewares/provider')

const UserController = require('./app/controllers/UserController')
const SessionController = require('./app/controllers/SessionController')
const DashboardController = require('./app/controllers/DashboardController')
const FileController = require('./app/controllers/FileController')

const ProcessController = require('./app/controllers/ProcessController')
const DocumentController = require('./app/controllers/DocumentController')
const CandidateController = require('./app/controllers/CandidateController')
const OcupationController = require('./app/controllers/OcupationController')

routes.use((req, res, next) => {
  res.locals.flashSuccess = req.flash('success')
  res.locals.flashError = req.flash('error')

  moment.locale('pt-br')

  if (numeral.locales['pt-br'] === undefined) {
    // load a locale
    numeral.register('locale', 'pt-br', {
      delimiters: {
        thousands: '.',
        decimal: ','
      },
      abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
      },
      ordinal: function (number) {
        return number === 1 ? 'c' : 'cent'
      },
      currency: {
        symbol: 'R$'
      }
    })
  }

  return next()
})

routes.get('/', guestMiddlware, CandidateController.home)

routes.get('/login', guestMiddlware, SessionController.create)

routes.post('/signin', SessionController.store)
routes.get('/signup', guestMiddlware, UserController.signup)
routes.post('/signup', upload.single('avatar'), UserController.store)

routes.get('/publish', guestMiddlware, ProcessController.publish)

routes.get('/files/:file', FileController.show)

routes.use('/app', authMiddlware, providerMiddleware)

routes.use('/candidate', guestMiddlware)

routes.get('/app/logout', SessionController.destroy)

routes.get('/app/dashboard', DashboardController.index)

/**
 * Candidate Subscription Endpoint
 */

routes.get('/candidate/home', CandidateController.home)
routes.get('/candidate/process/:id', CandidateController.index)
routes.get('/candidate/logout', SessionController.destroy)
routes.get('/candidate/reset', UserController.resetpassword)
routes.post('/candidate/verify', CandidateController.verifyCpf)
routes.get('/candidate/finish', CandidateController.getCandidate)
routes.get('/candidate/reset', UserController.reset)
routes.get('/candidate/degree/:id', OcupationController.getOcupationDegree)
routes.post(
  '/candidate/process/finish',
  upload.fields([
    { name: 'degreedoc' },
    { name: 'coursedoc' },
    { name: 'expdoc' }
  ]),
  CandidateController.store
)

/**
 * User Endpoint
 */

routes.get('/app/user', UserController.create)
routes.get('/app/user/getall', UserController.getUsers)
routes.get('/app/user/reset', UserController.reset)
routes.post('/app/user/reset/:id', UserController.resetPassword)
routes.post('/app/user', upload.single('avatar'), UserController.store)
routes.post('/app/user/:id', upload.single('avatar'), UserController.save)
routes.get('/app/user/edit/:id', UserController.edit)

/**
 * Process Endpoint
 */

routes.get('/app/process', ProcessController.index)

routes.post('/app/process/switch', ProcessController.switch)

routes.get('/app/process/all', ProcessController.getall)

routes.post('/app/process', ProcessController.store)

routes.post('/app/process/:id', ProcessController.save)

routes.get('/app/process/candidate/remove/:id', CandidateController.remove)

routes.post('/app/process/candidate/remove/:id', CandidateController.candidateInactive)

routes.get('/app/process/candidate/pending', CandidateController.listpending)

routes.get(
  '/app/process/candidate/classified',
  CandidateController.listclassified
)

routes.get(
  '/app/process/candidate/disqualified',
  CandidateController.listdisqualified
)

routes.get('/app/candidate/all/', CandidateController.getall)

routes.get(
  '/app/candidate/all/classified',
  CandidateController.getallClassified
)

routes.get(
  '/app/candidate/all/disqualified',
  CandidateController.getallDisqualified
)

routes.post('/app/degree/validate/:id', CandidateController.validateDegree)

routes.post('/app/course/validate/:id', CandidateController.validateCourse)

routes.post(
  '/app/experience/validate/:id',
  CandidateController.validateExperience
)

routes.post('/app/candidate/status/:id', CandidateController.changeStatus)

routes.post('/app/candidate/lock/:id', CandidateController.lockCandidate)

routes.post(
  '/app/candidate/rptclassificadoporcargo',
  CandidateController.rptListagemCandidatosClassificadosPorCargo
)

routes.get(
  '/app/candidate/rptclassificadoporcargo',
  CandidateController.viewRptListagemCandidatosClassificadosPorCargo
)

routes.post(
  '/app/candidate/rptdesclassificadoporcargo',
  CandidateController.rptListagemCandidatosDesclassificadosPorCargo
)

routes.get(
  '/app/candidate/rptdesclassificadoporcargo',
  CandidateController.viewRptListagemCandidatosDesclassificadosPorCargo
)

routes.post(
  '/app/candidate/check/:id',
  CandidateController.verifyClassifiedCandidate
)

/**
 * Document Endpoint
 */

routes.get('/app/document/process/:process_id', DocumentController.getall)

routes.post('/app/document/publish/:id', DocumentController.save)

routes.post(
  '/app/document/upload',
  upload.single('document'),
  DocumentController.store
)

/**
 * Ocupation Endpoint
 */

routes.get('/app/ocupation', OcupationController.create)

routes.get('/app/ocupation/list', OcupationController.list)
routes.get('/app/ocupation/all', OcupationController.getOcupations)

routes.post('/app/ocupation', OcupationController.store)

routes.get('/app/ocupation/edit/:id', OcupationController.edit)

routes.post('/app/ocupation/:id', OcupationController.save)

routes.use(function (req, res, next) {
  res.status(404).redirect('/')
})

module.exports = routes
