'use strict'

const uuid = require('uuid/v4') // ES5

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('processes', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: uuid()
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      start_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      start_hour: {
        type: Sequelize.STRING,
        allowNull: false
      },
      finish_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      finish_hour: {
        type: Sequelize.STRING,
        allowNull: false
      },
      is_published: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      step: {
        type: Sequelize.ENUM(
          'Cadastrando',
          'Inscrições',
          'Triagem ou seleção',
          'Contato com o candidato',
          'Entrevista individual',
          'Finalizado'
        ),
        allowNull: false,
        defaultValue: 'Cadastrando'
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('processes')
  }
}
