'use strict'

const uuid = require('uuid/v4') // ES5

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ocupations', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: uuid()
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      detail: {
        type: Sequelize.STRING,
        allowNull: false
      },
      degree: {
        type: Sequelize.ENUM(
          'Ensino Fundamental',
          'Ensino Médio',
          'Ensino Técnico',
          'Graduação',
          'Pós-Graduação',
          'Mestrado',
          'Doutorado',
          'Pós-Doutorado (Phd)'
        ),
        allowNull: false,
        defaultValue: 'Ensino Médio'
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ocupations')
  }
}
