'use strict'

const uuid = require('uuid/v4') // ES5
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('degree_docs', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: uuid()
      },
      filename: {
        type: Sequelize.STRING
      },
      mimetype: {
        type: Sequelize.STRING
      },
      originalname: {
        type: Sequelize.STRING
      },
      degreename: {
        type: Sequelize.STRING
      },
      degreenamecourse: {
        type: Sequelize.STRING
      },
      degreeconclusionyear: {
        type: Sequelize.STRING
      },
      is_valid: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ''
      },
      note: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: 'Não há'
      },
      candidate_id: {
        type: Sequelize.UUID
      },
      user_id: {
        type: Sequelize.UUID,
        references: { model: 'users', key: 'id' }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('degree_docs')
  }
}
