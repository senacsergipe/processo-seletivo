'use strict'

const uuid = require('uuid/v4') // ES5
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ocupation_processes', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: uuid()
      },
      process_id: {
        type: Sequelize.UUID,
        references: { model: 'processes', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      ocupation_id: {
        type: Sequelize.UUID,
        references: { model: 'ocupations', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ocupation_processes')
  }
}
