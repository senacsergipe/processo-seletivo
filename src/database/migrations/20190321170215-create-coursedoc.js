'use strict'

const uuid = require('uuid/v4') // ES5
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('course_docs', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: uuid()
      },
      filename: {
        type: Sequelize.STRING
      },
      mimetype: {
        type: Sequelize.STRING
      },
      originalname: {
        type: Sequelize.STRING
      },
      coursename: {
        type: Sequelize.STRING
      },
      courseconclusionyear: {
        type: Sequelize.STRING
      },
      courseworkload: {
        type: Sequelize.STRING
      },
      candidate_id: {
        type: Sequelize.UUID
      },
      is_valid: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ''
      },
      note: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: 'Não há'
      },
      user_id: {
        type: Sequelize.UUID,
        references: { model: 'users', key: 'id' }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('course_docs')
  }
}
