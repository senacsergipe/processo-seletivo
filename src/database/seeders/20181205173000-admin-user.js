'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'users',
      [
        {
          name: 'Senac',
          email: 'jean.cabral@se.senac.br',
          password_hash:
						'$2a$08$gDnpsCeR0STaQc79tLmfBOfqLfcK.qIc4oAXAGShVBfGB8sdRgByO',
          provider: true,
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {})
  }
}
