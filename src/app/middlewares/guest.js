module.exports = (req, res, next) => {
  if (req.session && !req.session.user) {
    res.locals.candidate = req.session.candidate
    return next()
  }

  return res.redirect('/app/dashboard')
}
