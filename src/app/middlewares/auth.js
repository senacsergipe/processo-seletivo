module.exports = (req, res, next) => {
  if (req.session && req.session.user) {
    res.locals.user = req.session.user

    res.locals.currentprocess = req.session.currentprocess
    res.locals.allprocesses = req.session.allprocesses

    console.log(req.session.processes)

    return next()
  }

  return res.redirect('/')
}
