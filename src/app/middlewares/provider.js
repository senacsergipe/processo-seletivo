module.exports = (req, res, next) => {
  if (!req.session.user.provider) {
    return res.redirect('/candidate/home')
  }
  res.locals.user = req.session.user
  return next()
}
