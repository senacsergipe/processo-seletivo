const { Process, Ocupation } = require('../models')
const Sequelize = require('sequelize')
class ProcessController {
  async index (req, res) {
    const process = await Process.findAll({
      include: [
        {
          model: Ocupation,
          as: 'ocupations',
          required: false,
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }
      ],
      order: [ [ 'id', 'DESC' ] ]
    })

    return res.render('processes/index', {
      process
    })
  }

  async store (req, res) {
    const {
      name,
      start_date,
      finish_date,
      start_hour,
      finish_hour,
      ocupation
    } = req.body[0]

    const process = await Process.create({
      name,
      start_date,
      finish_date,
      start_hour,
      finish_hour
    })

    Process.findByPk(process.dataValues.id).then(process => {
      process.addOcupations(ocupation).then(sc => {})

      return res.json(process)
    })
  }

  async switch (req, res) {
    const { process_id: processId } = req.body

    const process = await Process.findOne({ where: { id: processId } })

    if (!process) {
      // console.log('Usuário não encontrado')
      req.flash('error', 'Unidade não encontrado')
      return res.redirect('/app/dashboard')
    }

    req.session.currentprocess = process

    return res.redirect(req.headers.referer)
  }

  async publish (req, res) {
    const Op = Sequelize.Op
    const process = await Process.findAll({
      where: {
        start_date: {
          [Op.gte]: new Date()
        },
        is_published: { [Op.not]: true }
      }
    })

    return res.json(process)
  }

  async save (req, res) {
    await Process.update(
      { ...req.body[0] },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      return res.json(data)
    })
  }

  async getall (req, res) {
    const Op = Sequelize.Op

    const processes = await Process.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      include: [
        {
          model: Ocupation,
          as: 'ocupations',
          required: false,
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        }
      }
    })

    return res.json({
      last_page: Math.ceil(processes.count / 20),
      data: processes.rows
    })
  }
}

module.exports = new ProcessController()
