const { Document } = require('../models')
const _ = require('lodash')

class DocumentController {
  async index (req, res) {
    const process = await Document.findAll()

    return res.render('documents/index', {
      process
    })
  }

  async store (req, res) {
    if (_.isNil(req.file)) {
      return res.json({ error: 'Arquivo inválido ou vazio.' })
    } else {
      const { filename } = req.file

      await Document.create({ ...req.body, filename: filename }).then(function (
        data
      ) {
        return res.json(data)
      })
    }
  }

  async save (req, res) {
    await Document.update(
      { ...req.body },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      return res.json(data)
    })
  }

  async getall (req, res) {
    const data = await Document.findAll({
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['publish_date', 'DESC']
      ],
      where: {
        process_id: req.params.process_id
      }
    })

    return res.json(data)
  }
}

module.exports = new DocumentController()
