const { User } = require('../models')
const Sequelize = require('sequelize')
const _ = require('lodash')

class UserController {
  signup (req, res) {
    return res.render('auth/signup')
  }

  async create (req, res) {
    const users = await User.findAll({})

    return res.render('user/create', { users })
  }

  async store (req, res) {
    if (_.isNil(req.file)) {
      await User.create({ ...req.body })
    } else {
      const { filename } = req.file
      await User.create({ ...req.body, avatar: filename })
    }

    return res.redirect('/app/user')
  }

  async save (req, res) {
    if (_.isNil(req.file)) {
      await User.update({ ...req.body }, { where: { id: req.params.id } })
    } else {
      const { filename } = req.file

      await User.update(
        { ...req.body, avatar: filename },
        { where: { id: req.params.id } }
      )
    }

    return res.redirect('/app/user')
  }

  async destroy (req, res) {
    const { id } = req.params

    const person = await User.destroy({
      where: {
        id
      }
    })

    req.flash('success', `Usuário Excluído [${person}]`)
    return res.redirect('/app/user')
  }

  async edit (req, res) {
    const { id } = req.params

    const person = await User.findOne({
      where: {
        id: id
      }
    })

    return res.render('user/edit', { person })
  }

  async reset (req, res) {
    return res.render('user/change_password')
  }

  async resetpassword (req, res) {
    return res.render('user/change-password-candidate')
  }

  async resetPassword (req, res) {
    User.update(
      { password: req.body.password },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (user) {
      req.flash('success', 'A redefinição de senha foi bem-sucedida!')
      res.redirect('/app/dashboard')
    })
  }

  async getUsers (req, res) {
    const Op = Sequelize.Op

    const users = await User.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        }
      }
    })

    return res.json({
      last_page: Math.ceil(users.count / 20),
      data: users.rows
    })
  }
}

module.exports = new UserController()
