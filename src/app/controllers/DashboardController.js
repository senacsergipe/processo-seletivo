const { User, Candidate, Process } = require('../models')
const Sequelize = require('sequelize')
class DashboardController {
  async index (req, res) {
    // const Op = Sequelize.Op
    const profile = await User.findOne({ where: { id: req.session.user.id } })

    const process = await Process.findOne({
      where: {
        id: req.session.currentprocess.id
      }
    })

    const candidatestatus = await Candidate.sequelize
      .query('SELECT * FROM public.vw_status_candidate WHERE p = :idprocess', {
        replacements: {
          idprocess: req.session.currentprocess.id
        },
        type: Sequelize.QueryTypes.SELECT
      })
      .then(data => {
        console.log(data)
        return data
      })

    const subscriptions = await Candidate.sequelize
      .query(
        'SELECT t, x, y, p FROM public.vw_subscription_by_day WHERE p = :idprocess ORDER BY x',
        {
          replacements: {
            idprocess: req.session.currentprocess.id
          },
          type: Sequelize.QueryTypes.SELECT
        }
      )
      .then(data => {
        return data
      })

    const ocupations = await Candidate.sequelize
      .query(
        'SELECT t, p, x, y FROM public.vw_qty_per_ocupation WHERE p = :idprocess',
        {
          replacements: {
            idprocess: req.session.currentprocess.id
          },
          type: Sequelize.QueryTypes.SELECT
        }
      )
      .then(data => {
        return data
      })

    console.log(subscriptions)

    return res.render('dashboard', {
      profile,
      subscriptions,
      ocupations,
      candidatestatus,
      process
    })
  }
}

module.exports = new DashboardController()
