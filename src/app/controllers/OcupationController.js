const { Ocupation } = require('../models')
const Sequelize = require('sequelize')

class OcupationController {
  async create (req, res) {
    const ocupations = await Ocupation.findAll({})

    return res.render('ocupation/create', { ocupations })
  }

  async store (req, res) {
    await Ocupation.create({ ...req.body })

    return res.redirect('/app/ocupation')
  }

  async save (req, res) {
    await Ocupation.update({ ...req.body }, { where: { id: req.params.id } })

    return res.redirect('/app/ocupation')
  }

  async edit (req, res) {
    const { id } = req.params

    const ocupation = await Ocupation.findOne({
      where: {
        id: id
      }
    })

    return res.render('ocupation/edit', { ocupation })
  }

  async list (req, res) {
    const ocupations = await Ocupation.findAll()

    return res.render('ocupation/list', { ocupations })
  }

  async getOcupationDegree (req, res) {
    const ocupations = await Ocupation.findAll({
      where: {
        id: req.params.id
      }
    })
    return res.render('candidate/_degree', { ocupations })
  }

  async getOcupations (req, res) {
    const Op = Sequelize.Op

    const ocupations = await Ocupation.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        }
      }
    })

    return res.json({
      last_page: Math.ceil(ocupations.count / 20),
      data: ocupations.rows
    })
  }
}

module.exports = new OcupationController()
