const {
  Candidate,
  Process,
  Document,
  Ocupation,
  CourseDoc,
  ExperienceDoc,
  DegreeDoc,
  User,
  CandidateInactive
} = require('../models')
const Sequelize = require('sequelize')
const _ = require('lodash')

const Report = require('fluentreports').Report
const path = require('path')
const moment = require('moment')

class CandidateController {
  async home (req, res) {
    const processes = await Process.findAll({
      include: [
        {
          model: Document,
          as: 'document'
        }
      ],
      order: [ [ 'id', 'DESC' ] ]
    })

    return res.render('candidate/home', { processes })
  }

  async listpending (req, res) {
    const processes = await Process.findAll({
      include: [
        {
          model: Document,
          as: 'document'
        }
      ],
      where: {
        is_published: true
      }
    })

    return res.render('candidate/listpending', { processes })
  }

  async listclassified (req, res) {
    const processes = await Process.findAll({
      include: [
        {
          model: Document,
          as: 'document'
        }
      ],
      where: {
        is_published: true
      }
    })

    return res.render('candidate/listclassified', { processes })
  }

  async listdisqualified (req, res) {
    const processes = await Process.findAll({
      include: [
        {
          model: Document,
          as: 'document'
        }
      ],
      where: {
        is_published: true
      }
    })

    return res.render('candidate/listdisqualified', { processes })
  }

  async index (req, res) {
    console.log(!req.params)

    if (!req.params) {
      return res.redirect('/')
    }

    const processes = await Process.findOne({
      include: [
        {
          model: Ocupation,
          as: 'ocupations',
          required: false,
          attributes: ['id', 'name', 'degree'],
          through: { attributes: [] }
        }
      ],
      where: {
        id: req.params.id,
        is_published: true
      }
    })

    if (!processes) {
      return res.redirect('/')
    }

    console.log(processes)
    return res.render('candidate/index', {
      processes
    })
  }

  async create (req, res) {
    const candidates = await Candidate.findAll({})

    return res.render('candidate/create', { candidates })
  }

  async store (req, res) {
    try {
      if (req.files) {
        // console.log(req.files)
        let docs = []

        _.forEach(req.files, function (value, key) {
          docs.push(key)
        })

        console.log(_.includes(docs, 'coursedoc'))

        const degreedoc = req.files.degreedoc

        const expdoc = req.files.expdoc

        await Candidate.create({ ...req.body })
          .catch(error => {
            throw error
          })
          .then(candidate => {
            req.session.candidate = candidate.id
            /**
						|--------------------------------------------------
						| ANEXOS: ESCOLARIDADE
						|--------------------------------------------------
						*/
            let degreedocs = degreedoc

            let resdegreedocs = degreedocs.map((item, index) => {
              let props = Object.assign({}, item)

              props.candidate_id = candidate.id
              props.degreename = req.body.degree
              props.degreenamecourse = req.body.degreenamecourse
              props.degreeconclusionyear = req.body.degreeconclusionyear

              return props
            })

            console.log(resdegreedocs)

            DegreeDoc.bulkCreate(resdegreedocs, {
              individualHooks: true
            }).catch(error => {
              throw error
            })

            /**
						|--------------------------------------------------
						| ANEXOS: EXPERIENCIA NA AREA
						|--------------------------------------------------
						*/
            let expdocs = expdoc

            let resexpdocs = expdocs.map((item, index) => {
              let props = Object.assign({}, item)

              props.candidate_id = candidate.id
              props.expempresa = req.body.expempresa[index]
              props.expcargo = req.body.expcargo[index]
              props.expdataadm = req.body.expdataadm[index]
              props.expdatades = req.body.expdatades[index]

              return props
            })

            console.log(resexpdocs)

            ExperienceDoc.bulkCreate(resexpdocs, {
              individualHooks: true
            }).catch(error => {
              throw error
            })

            // CourseDoc.bulkCreate(resexpdocs, { individualHooks: true })

            /**
						|--------------------------------------------------
						| ANEXOS: QUALIF. PROFISSIONAL
						|--------------------------------------------------
						*/
            if (_.includes(docs, 'coursedoc')) {
              const coursedoc = req.files.coursedoc

              let coursedocs = coursedoc

              let rescoursedocs = coursedocs.map((item, index) => {
                let props = Object.assign({}, item)

                props.candidate_id = candidate.id
                props.coursename = req.body.coursename[index]
                props.courseconclusionyear =
									req.body.courseconclusionyear[index]
                props.courseworkload = req.body.courseworkload[index]

                return props
              })

              console.log(rescoursedocs)

              CourseDoc.bulkCreate(rescoursedocs, {
                individualHooks: true
              }).catch(error => {
                throw error
              })
            }
          })

        req.io.emit('locking', { msg: 'true' })

        return res
          .status(200)
          .json({ message: 'Cadastro Realizado com Sucesso' })
      } else {
        req.flash(
          'error',
          `Ocorreu um erro ao Realizar sua Inscrição. Tente novamente mais tarde!`
        )
        return res.redirect('/')
      }
    } catch (error) {
      return res.status(error.status).json({
        type: 'error',
        message: error.message
      })
    }
  }

  async changeStatus (req, res) {
    await Candidate.update(
      { ...req.body, user_id: req.session.user.id },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      req.io.emit('locking', { msg: 'true' })

      return res.json(data)
    })
  }

  async lockCandidate (req, res) {
    await Candidate.update(
      { ...req.body, user_id: req.session.user.id },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      req.io.emit('locking', { msg: 'true' })
      return res.json(data)
    })
  }

  async verifyCpf (req, res) {
    const cpf = await Candidate.findAndCountAll({
      where: {
        cpf: req.body.cpf,
        process_id: req.body.process_id
      }
    })

    return res.json({ cpf: cpf.count })
  }

  async verifyClassifiedCandidate (req, res) {
    const degrees = await DegreeDoc.findAndCountAll({
      where: {
        candidate_id: req.params.id,
        is_valid: 'Sim'
      }
    })

    const exps = await ExperienceDoc.findAndCountAll({
      where: {
        candidate_id: req.params.id,
        is_valid: 'Sim'
      }
    })

    console.log(degrees.count)
    console.log(exps.count)

    return res.json({ degree: degrees.count, exps: exps.count })
  }

  async validateDegree (req, res) {
    console.log(req.body)
    await DegreeDoc.update(
      { ...req.body, user_id: req.session.user.id },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      return res.json(data)
    })
  }

  async validateExperience (req, res) {
    console.log(req.body)
    await ExperienceDoc.update(
      { ...req.body, user_id: req.session.user.id },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      return res.json(data)
    })
  }

  async validateCourse (req, res) {
    console.log(req.body)
    await CourseDoc.update(
      { ...req.body, user_id: req.session.user.id },
      { where: { id: req.params.id }, individualHooks: true }
    ).then(function (data) {
      return res.json(data)
    })
  }

  async remove (req, res) {
    const { id } = req.params

    const person = await Candidate.findOne({
      where: {
        id: id
      }
    })

    return res.render('candidate/remove', { person })
  }

  async candidateInactive (req, res) {
    const { id } = req.params

    await CandidateInactive.create({ ...req.body, user_id: req.session.user.id }).then(function (data) {
      console.log({ ...req.body, user_id: req.session.user.id })

      const person = Candidate.destroy({
        where: {
          id: id
        }
      })

      req.flash('success', `Candidato Excluído [${person}]`)
      return res.render('candidate/listpending')
    })
  }

  async getCandidate (req, res) {
    if (!req.session.candidate) res.redirect('/')

    const candidate = await Candidate.findAll({
      where: {
        id: req.session.candidate
      },
      include: [
        {
          model: DegreeDoc,
          as: 'degreedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'degreename',
            'degreenamecourse',
            'degreeconclusionyear',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: CourseDoc,
          as: 'coursedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'coursename',
            'courseconclusionyear',
            'courseworkload',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: ExperienceDoc,
          as: 'experiencedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'expempresa',
            'expcargo',
            'expdataadm',
            'expdatades',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: Process,
          as: 'process',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Ocupation,
          as: 'ocupation',
          attributes: ['id', 'name']
        }
      ]
    })

    req.session.destroy(() => {
      res.clearCookie('senacsesel')
      return res.render('candidate/finish', { candidate })
    })
  }

  async getall (req, res) {
    const Op = Sequelize.Op

    const candidates = await Candidate.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      distinct: true,
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        },
        is_classified: false,
        is_disqualified: false,
        process_id: req.session.currentprocess.id
      },
      include: [
        {
          model: DegreeDoc,
          as: 'degreedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'degreename',
            'degreenamecourse',
            'degreeconclusionyear',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: CourseDoc,
          as: 'coursedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'coursename',
            'courseconclusionyear',
            'courseworkload',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: ExperienceDoc,
          as: 'experiencedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'expempresa',
            'expcargo',
            'expdataadm',
            'expdatades',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: Process,
          as: 'process',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: User,
          as: 'users',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Ocupation,
          as: 'ocupation',
          attributes: ['id', 'name']
        }
      ]
    })

    console.log(candidates.rows)

    return res.json({
      last_page: Math.ceil(candidates.count / 100),
      data: candidates.rows
    })
  }

  async getallClassified2 (req, res) {
    const candidates = await Candidate.sequelize.query(
      `SELECT
						candidate_name,
						candidate_cpf,
						candidates_rg,
						candidate_is_pcd,
						process_name,
						ocupation_name,
						user_name,
						updated_at,
						process_id,
						candidate_id,
						ocupation_id,
						qual_prof_points,
						exp_prof_points,
						sum_total_points,
						candidate_age,
						candidate_birthday,
						candidate_age_datail
				FROM public.vw_candidate_classified_process
						WHERE process_id = :idprocess LIMIT :limit OFFSET :offset`,

      {
        replacements: {
          idprocess: req.session.currentprocess.id,
          limit: req.query.size,
          offset: req.query.size * (req.query.page - 1)
        },
        type: Sequelize.QueryTypes.SELECT
      }
    )

    console.log(candidates)

    return res.json({
      last_page: Math.ceil(candidates.count / 100),
      data: candidates.rows
    })
  }

  async getallDisqualified (req, res) {
    const Op = Sequelize.Op

    const candidates = await Candidate.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      distinct: true,
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        },
        is_classified: false,
        is_lock: true,
        is_disqualified: true,
        process_id: req.session.currentprocess.id
      },
      include: [
        {
          model: DegreeDoc,
          as: 'degreedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'degreename',
            'degreenamecourse',
            'degreeconclusionyear',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: CourseDoc,
          as: 'coursedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'coursename',
            'courseconclusionyear',
            'courseworkload',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: ExperienceDoc,
          as: 'experiencedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'expempresa',
            'expcargo',
            'expdataadm',
            'expdatades',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: User,
          as: 'users',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Process,
          as: 'process',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Ocupation,
          as: 'ocupation',
          attributes: ['id', 'name']
        }
      ]
    })

    console.log(candidates.rows)

    return res.json({
      last_page: Math.ceil(candidates.count / 100),
      data: candidates.rows
    })
  }

  async getallClassified (req, res) {
    const Op = Sequelize.Op

    const candidates = await Candidate.findAndCountAll({
      limit: req.query.size,
      offset: req.query.size * (req.query.page - 1),
      distinct: true,
      order: [
        // Will escape title and validate DESC against a list of valid direction parameters
        ['name', 'ASC']
      ],
      where: {
        name: {
          [Op.iLike]: `%${
            typeof req.query.filters !== 'undefined'
              ? req.query.filters[0].value.toUpperCase()
              : ''
          }%`
        },
        is_classified: true,
        is_lock: true,
        is_disqualified: false,
        process_id: req.session.currentprocess.id
      },
      include: [
        {
          model: DegreeDoc,
          as: 'degreedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'degreename',
            'degreenamecourse',
            'degreeconclusionyear',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: CourseDoc,
          as: 'coursedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'coursename',
            'courseconclusionyear',
            'courseworkload',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: ExperienceDoc,
          as: 'experiencedocs',
          required: false,
          attributes: [
            'id',
            'filename',
            'expempresa',
            'expcargo',
            'expdataadm',
            'expdatades',
            'mimetype',
            'originalname',
            'is_valid',
            'note'
          ]
        },
        {
          model: User,
          as: 'users',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Process,
          as: 'process',
          required: false,
          attributes: ['id', 'name']
        },
        {
          model: Ocupation,
          as: 'ocupation',
          required: false,
          attributes: ['id', 'name']
        }
      ]
    })

    console.log(candidates.count)

    return res.json({
      last_page: Math.ceil(candidates.count / 100),
      data: candidates.rows
    })
  }

  async viewRptListagemCandidatosClassificadosPorCargo (req, res) {
    const ocupations = await Ocupation.findAll({})
    const processes = await Process.findAll({})
    return res.render('candidate/reportclassified', { ocupations, processes })
  }

  async rptListagemCandidatosClassificadosPorCargo (req, res) {
    const { process_id: processId, ocupation_id: ocupationId } = req.body
    console.log(ocupationId === '0')
    if (ocupationId === '0') {
      Candidate.sequelize
        .query(
          `SELECT candidate_name, candidates_rg, candidate_is_pcd, process_name, ocupation_name, process_id FROM public.vw_candidate_classified_process_rpt
				WHERE process_id = :idprocess`,
          {
            replacements: {
              idprocess: processId
            },
            type: Sequelize.QueryTypes.SELECT
          }
        )
        .then(data => {
          var counter = 0
          var detail = function (x, r) {
            counter++
            x.band(
              [
                { data: r.candidates_rg, width: 75, align: 1 },
                { data: r.candidate_name, width: 280 },
                { data: counter, width: 60, align: 3 },
                { data: r.candidate_is_pcd, width: 70, align: 3 }
              ],
              {
                x: 40,
                border: 0,
                padding: 5,
                width: 0,
                wrap: 1,
                fill: counter % 2 === 0 ? '#f0f0f0' : '#fff',
                textColor: '#000',
                fontsize: 8
              }
            )
          }

          var productTypeHeader = function (x, r) {
            x.newline()
            x.fontBold()
            x.band(
              [
                {
                  data: r.ocupation_name,
                  width: 310,
                  fontBold: true,
                  fontsize: 11
                }
              ],
              {
                x: 50
              }
            )
            x.newline()
            x.fontNormal()
          }

          var proposalHeader = function (x, r) {
            var fSize = 9
            x.print(
              'Senac - Serviço Nacional de Aprendizagem Comercial - Sergipe',
              {
                x: 20,
                fontsize: fSize
              }
            )
            x.print('Resultado do ' + r.process_name, {
              x: 60,
              y: 50,
              fontSize: fSize + 8,
              fontBold: true
            })
            x.print('Data: ' + moment().format('DD/MM/YYYY'), {
              x: 60,
              y: 80,
              fontsize: fSize + 2,
              fontBold: true
            })
            x.print('Aprovados em Ordem de Classificação', {
              x: 60,
              y: 100,
              fontsize: fSize,
              fontBold: true
            })
            x.newline()
            x.fontSize(8)
            x.band(
              [
                { data: 'R.G.', width: 75, align: 1 },
                { data: 'Nome Completo', width: 280 },
                { data: 'Classificação', width: 60, align: 3 },
                { data: 'PCD', width: 70, align: 3 }
              ],
              { x: 40 }
            )
            x.bandLine(1)
          }

          var report = new Report(
            './tmp/download/rptListagemCandidatosClassificados.pdf',
            { paper: 'A4' }
          ).data(data)

          report.margins(20).detail(detail)

          // See you can separate it; and chain however you need too
          report
            .groupBy('no')
            .header(proposalHeader)
            .groupBy('ocupation_name')
            .header(productTypeHeader)

          // Run the Report
          // displayReport is predefined to make it display in the browser
          report.render(function (Err, name) {
            const filePath = path.resolve(
              __dirname,
              '..',
              '..',
              '..',
              'tmp',
              'download',
              'rptListagemCandidatosClassificados.pdf'
            )
            return res.download(filePath, function (err) {
              if (err) {
                console.log('Error')
                console.log(err)
              } else {
                console.log('Success')
              }
            })
          })
        })
    } else {
      Candidate.sequelize
        .query(
          `SELECT candidate_name, candidates_rg, candidate_is_pcd, process_name, ocupation_name, process_id FROM public.vw_candidate_classified_process_rpt
				WHERE process_id = :idprocess and ocupation_id = :idocupation`,
          {
            replacements: {
              idprocess: processId,
              idocupation: ocupationId
            },
            type: Sequelize.QueryTypes.SELECT
          }
        )
        .then(data => {
          var counter = 0
          var detail = function (x, r) {
            counter++
            x.band(
              [
                { data: r.candidates_rg, width: 75, align: 1 },
                { data: r.candidate_name, width: 280 },
                { data: counter, width: 60, align: 3 },
                { data: r.candidate_is_pcd, width: 70, align: 3 }
              ],
              {
                x: 40,
                border: 0,
                padding: 5,
                width: 0,
                wrap: 1,
                fill: counter % 2 === 0 ? '#f0f0f0' : '#fff',
                textColor: '#000',
                fontsize: 8
              }
            )
          }

          var productTypeHeader = function (x, r) {
            x.newline()
            x.fontBold()
            x.band(
              [
                {
                  data: r.ocupation_name,
                  width: 310,
                  fontBold: true,
                  fontsize: 11
                }
              ],
              {
                x: 50
              }
            )
            x.newline()
            x.fontNormal()
          }

          var proposalHeader = function (x, r) {
            var fSize = 9
            x.print(
              'Senac - Serviço Nacional de Aprendizagem Comercial - Sergipe',
              {
                x: 20,
                fontsize: fSize
              }
            )
            x.print('Resultado do ' + r.process_name, {
              x: 60,
              y: 50,
              fontSize: fSize + 8,
              fontBold: true
            })
            x.print('Data: ' + moment().format('DD/MM/YYYY'), {
              x: 60,
              y: 80,
              fontsize: fSize + 2,
              fontBold: true
            })
            x.print('Aprovados em Ordem de Classificação', {
              x: 60,
              y: 100,
              fontsize: fSize,
              fontBold: true
            })
            x.newline()
            x.fontSize(8)
            x.band(
              [
                { data: 'R.G.', width: 75, align: 1 },
                { data: 'Nome Completo', width: 280 },
                { data: 'Classificação', width: 60, align: 3 },
                { data: 'PCD', width: 70, align: 3 }
              ],
              { x: 40 }
            )
            x.bandLine(1)
          }

          var report = new Report(
            './tmp/download/rptListagemCandidatosClassificados.pdf',
            { paper: 'A4' }
          ).data(data)

          report.margins(20).detail(detail)

          // See you can separate it; and chain however you need too
          report
            .groupBy('no')
            .header(proposalHeader)
            .groupBy('ocupation_name')
            .header(productTypeHeader)

          // Run the Report
          // displayReport is predefined to make it display in the browser
          report.render(function (Err, name) {
            const filePath = path.resolve(
              __dirname,
              '..',
              '..',
              '..',
              'tmp',
              'download',
              'rptListagemCandidatosClassificados.pdf'
            )
            return res.download(filePath, function (err) {
              if (err) {
                console.log('Error')
                console.log(err)
              } else {
                console.log('Success')
              }
            })
          })
        })
    }
  }

  async viewRptListagemCandidatosDesclassificadosPorCargo (req, res) {
    const ocupations = await Ocupation.findAll({})
    const processes = await Process.findAll({})
    return res.render('candidate/reportdisqualified', {
      ocupations,
      processes
    })
  }

  async rptListagemCandidatosDesclassificadosPorCargo (req, res) {
    const { process_id: processId, ocupation_id: ocupationId } = req.body
    console.log(ocupationId === '0')
    if (ocupationId === '0') {
      Candidate.sequelize
        .query(
          `SELECT name, doc, process_id, id, ocupation_name, candidate_name, note, birthday, cpf, rg, address, nro, neighborhood, city, postalcode, province, tel, email, pcd, pcdtipo, condesp, condespobs, is_lock, is_disqualified, is_classified, is_approved, ocupation_id, user_id, created_at, updated_at
					FROM public.vw_disqualified_notes where process_id = :idprocess ORDER BY candidate_name`,
          {
            replacements: {
              idprocess: processId
            },
            type: Sequelize.QueryTypes.SELECT
          }
        )
        .then(data => {
          var counter = 0
          var detail = function (x, r) {
            counter++
            x.band(
              [
                { data: r.doc, width: 75, align: 1 },
                { data: r.note, width: 380, align: 1 }
              ],
              {
                x: 40,
                border: 0,
                padding: 5,
                width: 0,
                wrap: 1,
                fill: counter % 2 === 0 ? '#f0f0f0' : '#fff',
                textColor: '#000',
                fontsize: 8
              }
            )
          }

          var productTypeHeader = function (x, r) {
            x.newline()
            x.fontBold()
            x.band(
              [
                {
                  data: r.candidate_name.toUpperCase() + ' - ' + r.rg,
                  width: 310,
                  fontBold: true,
                  fontsize: 11
                }
              ],
              {
                x: 50
              }
            )
            x.newline()
            x.fontNormal()
          }

          var proposalHeader = function (x, r) {
            var fSize = 9
            x.print(
              'Senac - Serviço Nacional de Aprendizagem Comercial - Sergipe',
              {
                x: 20,
                fontsize: fSize
              }
            )
            x.print(r.name, {
              x: 60,
              y: 50,
              fontSize: fSize + 8,
              fontBold: true
            })
            x.print('Data: ' + moment().format('DD/MM/YYYY'), {
              x: 60,
              y: 80,
              fontsize: fSize + 2,
              fontBold: true
            })
            x.print(
              'Cargo: ' + r.ocupation_name + ' | Situação: Desclassificados',
              {
                x: 60,
                y: 100,
                fontsize: fSize,
                fontBold: true
              }
            )
            x.newline()
            x.fontSize(8)
            x.band(
              [
                { data: 'Documento', width: 75, align: 1 },
                { data: 'Observação', width: 380 }
              ],
              { x: 40 }
            )
            x.bandLine(1)
          }

          var report = new Report(
            './tmp/download/rptListagemCandidatosDesclassificados.pdf',
            { paper: 'A4' }
          ).data(data)

          report.margins(20).detail(detail)

          // See you can separate it; and chain however you need too
          report
            .groupBy('no')
            .header(proposalHeader)
            .groupBy('candidate_name')
            .header(productTypeHeader)

          // Run the Report
          // displayReport is predefined to make it display in the browser
          report.render(function (Err, name) {
            const filePath = path.resolve(
              __dirname,
              '..',
              '..',
              '..',
              'tmp',
              'download',
              'rptListagemCandidatosDesclassificados.pdf'
            )
            return res.download(filePath, function (err) {
              if (err) {
                console.log('Error')
                console.log(err)
              } else {
                console.log('Success')
              }
            })
          })
        })
    } else {
      Candidate.sequelize
        .query(
          `SELECT name, doc, process_id, ocupation_name, id, candidate_name, note, birthday, cpf, rg, address, nro, neighborhood, city, postalcode, province, tel, email, pcd, pcdtipo, condesp, condespobs, is_lock, is_disqualified, is_classified, is_approved, ocupation_id, user_id, created_at, updated_at
					FROM public.vw_disqualified_notes where process_id = :idprocess and ocupation_id = :idocupation ORDER BY candidate_name`,
          {
            replacements: {
              idprocess: processId,
              idocupation: ocupationId
            },
            type: Sequelize.QueryTypes.SELECT
          }
        )
        .then(data => {
          var counter = 0
          var detail = function (x, r) {
            counter++
            x.band(
              [
                { data: r.doc, width: 75, align: 1 },
                { data: r.note, width: 380, align: 1 }
              ],
              {
                x: 40,
                border: 0,
                padding: 5,
                width: 0,
                wrap: 1,
                fill: counter % 2 === 0 ? '#f0f0f0' : '#fff',
                textColor: '#000',
                fontsize: 8
              }
            )
          }

          var productTypeHeader = function (x, r) {
            x.newline()
            x.fontBold()
            x.band(
              [
                {
                  data: r.candidate_name.toUpperCase() + ' - ' + r.rg,
                  width: 310,
                  fontBold: true,
                  fontsize: 11
                }
              ],
              {
                x: 50
              }
            )
            x.newline()
            x.fontNormal()
          }

          var proposalHeader = function (x, r) {
            var fSize = 9
            x.print(
              'Senac - Serviço Nacional de Aprendizagem Comercial - Sergipe',
              {
                x: 20,
                fontsize: fSize
              }
            )
            x.print(r.name, {
              x: 60,
              y: 50,
              fontSize: fSize + 8,
              fontBold: true
            })
            x.print('Data: ' + moment().format('DD/MM/YYYY'), {
              x: 60,
              y: 80,
              fontsize: fSize + 2,
              fontBold: true
            })
            x.print(
              'Cargo: ' + r.ocupation_name + ' | Situação: Desclassificados',
              {
                x: 60,
                y: 100,
                fontsize: fSize,
                fontBold: true
              }
            )
            x.newline()
            x.fontSize(8)
            x.band(
              [
                { data: 'Documento', width: 75, align: 1 },
                { data: 'Observação', width: 380, align: 1 }
              ],
              { x: 40 }
            )
            x.bandLine(1)
          }

          var report = new Report(
            './tmp/download/rptListagemCandidatosDesclassificados.pdf',
            { paper: 'A4' }
          ).data(data)

          report.margins(20).detail(detail)

          // See you can separate it; and chain however you need too
          report
            .groupBy('no')
            .header(proposalHeader)
            .groupBy('candidate_name')
            .header(productTypeHeader)

          // Run the Report
          // displayReport is predefined to make it display in the browser
          report.render(function (Err, name) {
            const filePath = path.resolve(
              __dirname,
              '..',
              '..',
              '..',
              'tmp',
              'download',
              'rptListagemCandidatosDesclassificados.pdf'
            )
            return res.download(filePath, function (err) {
              if (err) {
                console.log('Error')
                console.log(err)
              } else {
                console.log('Success')
              }
            })
          })
        })
    }
  }
}

module.exports = new CandidateController()
