const { User, Process } = require('../models')
class SessionController {
  async create (req, res) {
    return res.render('auth/signin')
  }

  async store (req, res) {
    const { email, password } = req.body

    const user = await User.findOne({ where: { email } })

    if (!user) {
      req.flash('error', 'Usuário não encontrado')
      return res.redirect('/')
    }

    if (!(await user.checkPassword(password))) {
      req.flash('error', 'Senha incorreta')
      return res.redirect('/')
    }

    const defaultprocess = await Process.findOne({})

    const processes = await Process.findAll({})

    req.session.user = user
    req.session.currentprocess = defaultprocess
    req.session.allprocesses = processes

    return res.redirect('/app/dashboard')
  }

  destroy (req, res) {
    req.session.destroy(() => {
      res.clearCookie('senacsesel')
      return res.redirect('/')
    })
  }
}

module.exports = new SessionController()
