const uuid = require('uuid/v4')
const { User } = require('../models')

module.exports = (sequelize, DataTypes) => {
  const Candidate = sequelize.define('Candidate', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    },
    name: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    cpf: DataTypes.STRING,
    rg: DataTypes.STRING,
    address: DataTypes.STRING,
    nro: DataTypes.STRING,
    neighborhood: DataTypes.STRING,
    city: DataTypes.STRING,
    postalcode: DataTypes.STRING,
    province: DataTypes.STRING,
    tel: DataTypes.STRING,
    email: DataTypes.STRING,
    pcd: DataTypes.STRING,
    pcdtipo: DataTypes.STRING,
    condesp: DataTypes.STRING,
    condespobs: DataTypes.STRING,
    is_disqualified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_lock: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_classified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_approved: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }, // Set FK relationship (hasMany) with `User`
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: 'id'
      }
    },
    process_id: DataTypes.UUID,
    ocupation_id: DataTypes.UUID
  })

  Candidate.associate = models => {
    Candidate.belongsTo(models.Process, {
      as: 'process',
      foreignKey: 'process_id'
    })

    Candidate.belongsTo(models.Ocupation, {
      as: 'ocupation',
      foreignKey: 'ocupation_id'
    })

    Candidate.hasMany(models.DegreeDoc, {
      as: 'degreedocs',
      foreignKey: 'candidate_id'
    })

    Candidate.hasMany(models.CourseDoc, {
      as: 'coursedocs',
      foreignKey: 'candidate_id'
    })

    Candidate.hasMany(models.ExperienceDoc, {
      as: 'experiencedocs',
      foreignKey: 'candidate_id'
    })

    Candidate.belongsTo(models.User, {
      as: 'users',
      foreignKey: 'user_id'
    })
  }

  Candidate.beforeCreate((candidate, _) => {
    return (candidate.id = uuid())
  })

  return Candidate
}
