'use strict'
const uuid = require('uuid/v4')
const { User } = require('../models')
module.exports = (sequelize, DataTypes) => {
  const DegreeDoc = sequelize.define(
    'DegreeDoc',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v1()'),
        type: DataTypes.UUID
      },
      filename: DataTypes.STRING,
      mimetype: DataTypes.STRING,
      originalname: DataTypes.STRING,
      candidate_id: DataTypes.UUID,
      degreename: DataTypes.STRING,
      degreenamecourse: DataTypes.STRING,
      degreeconclusionyear: DataTypes.STRING,
      is_valid: {
        type: DataTypes.STRING,
        defaultValue: '',
        allowNull: true
      },
      note: {
        type: DataTypes.STRING,
        defaultValue: 'Não há',
        allowNull: true
      }, // Set FK relationship (hasMany) with `User`
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: User,
          key: 'id'
        }
      }
    },
    {}
  )
  DegreeDoc.associate = function (models) {
    DegreeDoc.belongsTo(models.Candidate, {
      as: 'candidate',
      foreignKey: 'candidate_id'
    })
  }

  DegreeDoc.beforeCreate((degreedoc, _) => {
    return (degreedoc.id = uuid())
  })
  return DegreeDoc
}
