const uuid = require('uuid/v4')

module.exports = (sequelize, DataTypes) => {
  const Process = sequelize.define('Process', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    },
    name: DataTypes.STRING,
    start_date: DataTypes.DATEONLY,
    start_hour: DataTypes.STRING,
    finish_date: DataTypes.STRING,
    finish_hour: DataTypes.STRING,
    is_published: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    step: DataTypes.ENUM(
      'Cadastrando',
      'Inscrições',
      'Triagem ou seleção',
      'Contato com o candidato',
      'Entrevista individual',
      'Finalizado'
    )
  })

  Process.associate = models => {
    Process.hasMany(models.Document, {
      as: 'document',
      foreignKey: 'process_id'
    })

    Process.belongsToMany(models.Ocupation, {
      through: 'OcupationProcess',
      as: 'ocupations',
      foreignKey: 'process_id'
    })
  }

  Process.beforeCreate((process, _) => {
    return (process.id = uuid())
  })

  return Process
}
