const uuid = require('uuid/v4')
const { User } = require('../models')

module.exports = (sequelize, DataTypes) => {
  const CandidateInactive = sequelize.define('CandidateInactive', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    },
    name: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    cpf: DataTypes.STRING,
    rg: DataTypes.STRING,
    tel: DataTypes.STRING,
    email: DataTypes.STRING,
    reason: DataTypes.STRING,
    process_id: DataTypes.UUID,
    ocupation_id: DataTypes.UUID,
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: 'id'
      }
    }
  })

  CandidateInactive.associate = models => {
    CandidateInactive.belongsTo(models.Process, {
      as: 'process',
      foreignKey: 'process_id'
    })

    CandidateInactive.belongsTo(models.Ocupation, {
      as: 'ocupation',
      foreignKey: 'ocupation_id'
    })

    CandidateInactive.belongsTo(models.User, {
      as: 'users',
      foreignKey: 'user_id'
    })
    CandidateInactive.beforeCreate((candidate, _) => {
      return (candidate.id = uuid())
    })
  }
  return CandidateInactive
}
