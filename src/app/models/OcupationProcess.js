'use strict'

const uuid = require('uuid/v4')

module.exports = (sequelize, DataTypes) => {
  const OcupationProcess = sequelize.define('OcupationProcess', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    }
  })

  OcupationProcess.beforeCreate((processocup, _) => {
    return (processocup.id = uuid())
  })

  return OcupationProcess
}
