const uuid = require('uuid/v4')

module.exports = (sequelize, DataTypes) => {
  const Document = sequelize.define('Document', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    },
    name: DataTypes.STRING,
    filename: DataTypes.STRING,
    publish_date: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    is_published: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    process_id: DataTypes.UUID
  })

  Document.associate = models => {
    Document.belongsTo(models.Process, {
      as: 'process',
      foreignKey: 'process_id'
    })
  }

  Document.beforeCreate((document, _) => {
    return (document.id = uuid())
  })

  return Document
}
