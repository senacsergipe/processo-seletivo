'use strict'
const uuid = require('uuid/v4')
const { User } = require('../models')
module.exports = (sequelize, DataTypes) => {
  const ExperienceDoc = sequelize.define(
    'ExperienceDoc',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v1()'),
        type: DataTypes.UUID
      },
      filename: DataTypes.STRING,
      mimetype: DataTypes.STRING,
      originalname: DataTypes.STRING,
      candidate_id: DataTypes.UUID,
      expempresa: DataTypes.STRING,
      expcargo: DataTypes.STRING,
      expdataadm: DataTypes.DATEONLY,
      expdatades: DataTypes.DATEONLY,
      is_valid: {
        type: DataTypes.STRING,
        defaultValue: '',
        allowNull: true
      },
      note: {
        type: DataTypes.STRING,
        defaultValue: 'Não há',
        allowNull: true
      }, // Set FK relationship (hasMany) with `User`
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: User,
          key: 'id'
        }
      }
    },
    {}
  )
  ExperienceDoc.associate = function (models) {
    ExperienceDoc.belongsTo(models.Candidate, {
      as: 'candidate',
      foreignKey: 'candidate_id'
    })
  }

  ExperienceDoc.beforeCreate((experiencedoc, _) => {
    return (experiencedoc.id = uuid())
  })
  return ExperienceDoc
}
