'use strict'
const { User } = require('../models')

const uuid = require('uuid/v4')

module.exports = (sequelize, DataTypes) => {
  const CourseDoc = sequelize.define(
    'CourseDoc',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v1()'),
        type: DataTypes.UUID
      },
      filename: DataTypes.STRING,
      mimetype: DataTypes.STRING,
      originalname: DataTypes.STRING,
      candidate_id: DataTypes.UUID,
      coursename: DataTypes.STRING,
      courseconclusionyear: DataTypes.STRING,
      courseworkload: DataTypes.STRING,
      is_valid: {
        type: DataTypes.STRING,
        defaultValue: '',
        allowNull: true
      },
      note: {
        type: DataTypes.STRING,
        defaultValue: 'Não há',
        allowNull: true
      }, // Set FK relationship (hasMany) with `User`
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: User,
          key: 'id'
        }
      }
    },
    {}
  )
  CourseDoc.associate = function (models) {
    CourseDoc.belongsTo(models.Candidate, {
      as: 'candidate',
      foreignKey: 'candidate_id'
    })
  }

  CourseDoc.beforeCreate((coursedoc, _) => {
    return (coursedoc.id = uuid())
  })
  return CourseDoc
}
