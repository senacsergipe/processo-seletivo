'use strict'

const uuid = require('uuid/v4')

module.exports = (sequelize, DataTypes) => {
  const Ocupation = sequelize.define('Ocupation', {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v1()'),
      type: DataTypes.UUID
    },
    name: DataTypes.STRING,
    detail: DataTypes.STRING,
    degree: DataTypes.ENUM(
      'Ensino Fundamental',
      'Ensino Médio',
      'Ensino Técnico',
      'Graduação',
      'Pós-Graduação',
      'Mestrado',
      'Doutorado',
      'Pós-Doutorado (Phd)'
    )
  })

  Ocupation.associate = models => {
    Ocupation.belongsToMany(models.Process, {
      through: 'OcupationProcess'
    })
  }

  Ocupation.beforeCreate((ocupation, _) => {
    return (ocupation.id = uuid())
  })

  return Ocupation
}
