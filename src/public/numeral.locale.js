// load a locale
if (numeral.locales['pt-br'] === undefined) {
  numeral.register('locale', 'pt-br', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'k',
      million: 'm',
      billion: 'b',
      trillion: 't'
    },
    ordinal: function (number) {
      return number === 1 ? 'c' : 'cent'
    },
    currency: {
      symbol: 'R$'
    }
  })
}
