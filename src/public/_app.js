
function removeQualProf (comp) {
  $(comp).remove()
  let cont = Number(localStorage.getItem('contcourse'))
  cont = cont - 1
  localStorage.setItem('contcourse', `${cont}`)
}

$(function () {
  $('#adicQualProf').click(function (e) {
    e.preventDefault()
    let cont = Number(localStorage.getItem('contcourse'))
    if (cont === 6) {
      swal('Máximo permitido!')
      return false
    }
    $('#fst-qual-prof').append(`
				<fieldset id='fst-qual-prof-${cont}'>
					<div class='flex-grid'>
							<div>
									<h3>Curso ${cont}<span class="close" onclick="removeQualProf('#fst-qual-prof-${cont}')">&times;</span></h3>
									<p>
											<input placeholder='Nome do Curso' oninput=\"this.className = ''\" name='coursename[]'><input placeholder='Ano de Conclusão' oninput=\"this.className = 'courseconclusionyear'\" name='courseconclusionyear[]'><input placeholder='Carga Horária' oninput=\"this.className = 'courseworkload'\" name='courseworkload[]'>
									</p>
									<p>
											<label for='coursedoc'>Comprovante (*.pdf ou *.jpg)</label>
									</p>
									<p>
											<input onchange='ValidateSize(this)' type='file' name='coursedoc' placeholder='Selecionar Assinatura'/>
									</p>
							</div>
					</div>
				</fieldset>`)
    cont = cont + 1
    localStorage.setItem('contcourse', `${cont}`)
  })
})

function removeExp (comp) {
  $(comp).remove()
  let cont = Number(localStorage.getItem('contexp'))
  cont = cont - 2
  localStorage.setItem('contexp', `${cont}`)
}

$(function () {
  $('#adicExpArea').click(function (e) {
    let cont = Number(localStorage.getItem('contexp'))
    e.preventDefault()
    var numberOfSpans = $('#exp')
      .children('fieldset')
      .length
    if (numberOfSpans == 10) {
      swal('Máximo permitido!')
      return false
    }

    $('fieldset:last').after(`
				<fieldset id='fst-exp-${cont}'>
						<div class='flex-grid'>
								<div>
										<h3>Experiência ${numberOfSpans + 1}<span class="close" onclick="removeExp('#fst-exp-${cont}')">&times;</span></h3>
												<p>
														<input placeholder='Empresa' oninput=\"this.className = ''\" name='expempresa[]'><input placeholder='Cargo/Função' oninput=\"this.className = ''\" name='expcargo[]'>
														<input placeholder='Data Admissão' id='expdt${cont + 1}' oninput=\"this.className = ''\" name='expdataadm[]'>
														<input placeholder='Data de Desligamento' id='expdt${cont + 2}' class='datetimepicker' oninput=\"this.className = ''\" name='expdatades[]'>
												</p>
												<p>
														<label for='expdoc'>Comprovante (*.pdf ou *.jpg)</label>
												</p>
												<p>
														<input onchange='ValidateSize(this)' type='file' name='expdoc' placeholder='Selecionar Assinatura'/>
												</p>
								</div>
						</div>
				</fieldset>`)
    cont = cont + 2
    localStorage.setItem('contexp', `${cont}`)
  })
})

var modal = document.getElementById('modal-candidate')

$('#myBar').hide()

function ValidateSize (file) {
  if (/\.(jpe?g|pdf)$/i.test(file.files[0].name) === false) {
    swal('Arquivo de extensão inválida. Tipo de arquivos permitidos JPG ou PDF.')
    $(file).val('')
    return false
  }

  var FileSize = file
    .files[0]
    .size / 1024 / 1024 // in MB

  if (FileSize > 2) {
    swal('O arquivo informado excede o limite de 2 MB (megabytes)')
    $(file).val('')
    return false
  }
}

flatpickr('.datetimepicker', {
  allowInput: true,
  dateFormat: 'Y-m-d',
  altFormat: 'd/m/Y',
  altInput: true,
  'locale': 'pt',
  onReady: function () {
    var flatPickrInstance = this
    var $flatPickrInput = $(flatPickrInstance.element)
    $flatPickrInput
      .siblings('span')
      .click(function () {
        flatPickrInstance.toggle()
      })
  },
  onChange: function () {
    var x = document.getElementsByClassName('datetimepicker')
    var i
    for (i = 0; i < x.length; i++) {
      x[i]
        .classList
        .remove('invalid')
    }
  }
})

$(document).ready(function () {
  $('.cpf').mask('000.000.000-00', { reverse: true })

  $('.date').mask('00/00/0000')

  $('.cep').mask('00000-000')

  $('.phone_with_ddd').mask('(00) 0000-0000')

  handleDOBChanged()

  validaEmail()

  valida_cpf()

  $(document).on('focus', '#expdt0', function () {
    flatpickr('#expdt0', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',
      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt1', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt1', function () {
    flatpickr('#expdt1', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt0 = document.querySelector('#expdt0') // flatpickr
        var a = moment(expdt0.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Desligamento deve ser igual ou superior a ${moment(expdt0.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt2', function () {
    flatpickr('#expdt2', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt3', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt3', function () {
    flatpickr('#expdt3', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt2 = document.querySelector('#expdt2') // flatpickr
        var a = moment(expdt2.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt2.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt4', function () {
    flatpickr('#expdt4', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt5', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt5', function () {
    flatpickr('#expdt5', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt4 = document.querySelector('#expdt4') // flatpickr
        var a = moment(expdt4.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt4.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt6', function () {
    flatpickr('#expdt6', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt7', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt7', function () {
    flatpickr('#expdt7', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt6 = document.querySelector('#expdt6') // flatpickr
        var a = moment(expdt6.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt6.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt8', function () {
    flatpickr('#expdt8', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt9', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt9', function () {
    flatpickr('#expdt9', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt8 = document.querySelector('#expdt8') // flatpickr
        var a = moment(expdt8.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt8.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt10', function () {
    flatpickr('#expdt10', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt11', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt11', function () {
    flatpickr('#expdt11', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt10 = document.querySelector('#expdt10') // flatpickr
        var a = moment(expdt10.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt10.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt12', function () {
    flatpickr('#expdt12', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt13', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt13', function () {
    flatpickr('#expdt13', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt12 = document.querySelector('#expdt12') // flatpickr
        var a = moment(expdt12.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt12.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt14', function () {
    flatpickr('#expdt14', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt15', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt15', function () {
    flatpickr('#expdt15', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt14 = document.querySelector('#expdt14') // flatpickr
        var a = moment(expdt14.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt14.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt16', function () {
    flatpickr('#expdt16', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt17', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt17', function () {
    flatpickr('#expdt17', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt16 = document.querySelector('#expdt16') // flatpickr
        var a = moment(expdt16.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt16.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })

  $(document).on('focus', '#expdt18', function () {
    flatpickr('#expdt18', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        const fp = flatpickr('#expdt19', {}) // flatpickr
        fp.destroy()
      }
    })
  })

  $(document).on('focus', '#expdt19', function () {
    flatpickr('#expdt19', {
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',

      maxDate: 'today',
      altInput: true,
      'locale': 'pt',
      defaultDate: new Date(),
      onReady: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        $flatPickrInput
          .siblings('span')
          .click(function () {
            flatPickrInstance.toggle()
          })
      },
      onChange: function () {
        var flatPickrInstance = this
        var $flatPickrInput = $(flatPickrInstance.element)
        var expdt18 = document.querySelector('#expdt18') // flatpickr
        var a = moment(expdt18.value)
        var d = moment($flatPickrInput[0].value).add(1, 'days')
        result = d.diff(a, 'days')
        if (result < 365) {
          swal(`O período de Experiência Profissional não pode ser menor que 12 meses. A data de Deslisgamento deve ser igual ou superior a ${moment(expdt18.value).add(364, 'days').format('DD/MM/YYYY')}`)
          flatPickrInstance.clear()
        }
      }
    })
  })
})

var currentTab = 0 // Current tab is set to be the first tab (0)
showTab(currentTab) // Display the current tab

function showTab (n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName('tab')
  x[n].style.display = 'block'
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document
      .getElementById('prevBtn')
      .style
      .display = 'none'
    document
      .getElementById('submitBtn')
      .style
      .display = 'none'
  } else {
    document
      .getElementById('prevBtn')
      .style
      .display = 'inline'
  }
  if (n == (x.length - 1)) {
    document
      .getElementById('nextBtn')
      .style
      .display = 'none'
    document
      .getElementById('submitBtn')
      .style
      .display = 'inline'
  } else {
    document
      .getElementById('nextBtn')
      .style
      .display = 'inline'
    document
      .getElementById('nextBtn')
      .innerHTML = 'Avançar'
    document
      .getElementById('submitBtn')
      .style
      .display = 'none'
  }
  // ... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function sendData () {
  modal.style.display = 'block'

  let formData = new FormData()

  let form = document.getElementById('regForm')

  Array
    .prototype
    .forEach
    .call(form.querySelectorAll('input[type=file]'), function (input, i) {
      // use the input name, don't invent another one
      if (input.value) { formData.append(input.name, input.files[0]) }
    }
    )

  let params = $('#regForm').serializeArray()

  $.each(params, function (i, val) {
    if (val.name === 'birthday') {
      let data = moment(val.value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      formData.append('birthday', `'${data}'`)
    } else {
      formData.append(val.name, val.value)
    }
  })

  //	var xhr = new XMLHttpRequest;
  //	xhr.open('POST', '/echo/html/', true);
  //	xhr.send(formData);

  // Request
  var xhr = new XMLHttpRequest()
  xhr.open('post', '/candidate/process/finish', true)
  xhr.upload.onprogress = function (ev) {
    var percent = 0
    $('#myBar').show()
    if (ev.lengthComputable) {
      percent = 100 * ev.loaded / ev.total
      $('#myBar').width(percent + '%')
      // or something like progress tip
    }
  }

  xhr.onload = function (oEvent) {
    if (xhr.status == 200) {
      // go on
      window
        .location
        .replace('/candidate/finish')
    } else {
      modal.style.display = 'none'
      swal('Ocorreu um erro. Verifique os dados informados. (' + xhr.status + ')')
      console.error(xhr.statusText)
    }
  }

  xhr.onerror = function (e) {
    console.error(xhr.statusText)
  }

  xhr.send(formData)
}

function nextPrev (n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName('tab')
  // Exit the function if any field in the current tab is invalid:

  if (n == 1 && !validateForm()) { return false }

  // Hide the current tab:
  x[currentTab].style.display = 'none'
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n
  // if you have reached the end of the form...

  // Otherwise, display the correct tab:
  $('html, body').animate({
    scrollTop: $('.wrapper')
      .position()
      .top
  }, 'slow')

  localStorage.setItem('aba', currentTab)
  showTab(currentTab)
}

function validateForm () {
  // This function deals with validation of the form fields
  var x
  var y
  var s
  var i
  var valid = true
  x = document.getElementsByClassName('tab')
  y = x[currentTab].getElementsByTagName('input')
  s = x[currentTab].getElementsByTagName('select')

  let aba = Number(localStorage.getItem('aba'))

  if (aba == '2') {
    var x = document.getElementsByClassName('courseconclusionyear')
    var i
    for (i = 0; i < x.length; i++) {
      let year = Number(x[i].value)
      if (isNaN(year)) {
        x[i].className += ' invalid'
        x[i].value = ''
        valid = false
        swal('Digite um Ano Válido')
      }

      if (Number(x[i].value) < 2009) {
        x[i].className += ' invalid'
        x[i].value = ''
        valid = false
        swal('Os cursos devem ter sido realizados entre 2009 à 2019.')
      }
    }

    var x = document.getElementsByClassName('courseworkload')
    var i
    for (i = 0; i < x.length; i++) {
      let ch = Number(x[i].value)
      if (isNaN(ch)) {
        x[i].className += ' invalid'
        x[i].value = ''
        valid = false
        swal('Digite um CH válido')
      }

      if (Number(x[i].value) < 40) {
        x[i].className += ' invalid'
        x[i].value = ''
        valid = false
        swal('Os cursos devem ter carga horária mínima de 40 horas.')
      }
    }
  }

  // LOOP CAMPOS INPUT
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == '') {
      // add an "invalid" class to the field:
      y[i].className += ' invalid'
      // and set the current valid status to false
      valid = false // desenvolvimento para producao marque false
    } else {
      y[i]
        .classList
        .remove('invalid')
    }
  }

  // LOOP CAMPOS SELECT
  for (i = 0; i < s.length; i++) {
    var sel = s[i]

    var opt = sel.options[sel.selectedIndex]

    if (opt.value == '' && currentTab == 0) {
      sel.style.backgroundColor = '#ffdbdc'

      swal('Selecione o Cargo pretendido!')
      // and set the current valid status to false
      valid = false // desenvolvimento para producao marque false
    } else {
      sel.style.backgroundColor = 'transparent'
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document
      .getElementsByClassName('step')[currentTab]
      .className += ' finish'
  }
  return valid // return the valid status
}

function valida_cpf () {
  $('#cpf').on('change', function () {
    // Garante que o valor é uma string
    valor = $('#cpf').val()

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '')
    console.log(valor)
    // Verifica CPF, se não tiver tamanho 11, retorna false
    if (!(valor.length === 11)) {
      document
        .getElementById('cpf')
        .className += ' invalid'
      swal('CPF inválido')
      document
        .getElementById('cpf')
        .value = ''
      return false
    }

    var Soma
    var Resto
    Soma = 0

    if (valor == '00000000000') { return false }

    for (i = 1; i <= 9; i++) { Soma = Soma + parseInt(valor.substring(i - 1, i)) * (11 - i) }

    Resto = (Soma * 10) % 11

    if ((Resto == 10) || (Resto == 11)) { Resto = 0 }

    if (Resto != parseInt(valor.substring(9, 10))) {
      console.log('inválido')
      document
        .getElementById('cpf')
        .className += ' invalid'
      swal('CPF inválido')
      document
        .getElementById('cpf')
        .value = ''
      return false
    }

    Soma = 0

    for (i = 1; i <= 10; i++) { Soma = Soma + parseInt(valor.substring(i - 1, i)) * (12 - i) }

    Resto = (Soma * 10) % 11

    if ((Resto == 10) || (Resto == 11)) { Resto = 0 }

    if (Resto != parseInt(valor.substring(10, 11))) {
      document
        .getElementById('cpf')
        .className += ' invalid'
      swal('CPF inválido')
      document
        .getElementById('cpf')
        .value = ''
      console.log('inválido')
      return false
    }

    console.log('Validou! Verificando cadastro no processo pretendido...')

    let data = {
      'cpf': $('#cpf').val(),
      'process_id': localStorage.getItem('process_id')
    }

    let headers = {
      'Content-Type': 'application/json',
      'Access-Control-Origin': '*'
    }

    fetch(`/candidate/verify`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(data)
    })
      .then(function (response) {
        return response.json()
      })
      .then(function (data) {
        console.log(data)

        if (data.cpf > 0) {
          document
            .getElementById('cpf')
            .className += ' invalid'
          swal('Candidato', 'O CPF informado já possui registro neste Processo Seletivo Simplificado!', 'info')
          document
            .getElementById('cpf')
            .value = ''
          console.log('inválido')
          return false
        }

        return data
      })

    return true
  })
};

function validaEmail () {
  $('#email').on('change', function () {
    er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/
    if (!er.exec($('#email').val())) {
      valid = false
      document
        .getElementById('email')
        .className += ' invalid'
      swal('E-mail inválido')
      document
        .getElementById('email')
        .value = ''
    }
  })
}

// listener on date of birth field
function handleDOBChanged () {
  $('#birthday').on('change', function () {
    if (isDate($('#birthday').val())) {
      var age = calculateAge(parseDate($('#birthday').val()), new Date())
      if (age < 18) {
        document
          .getElementById('birthday')
          .className += ' invalid'
        swal('O candidato deverá ter 18 anos para participar do concurso.')
        document
          .getElementById('birthday')
          .className += ' invalid'
        document
          .getElementById('birthday')
          .value = ''
      } else {
        document
          .getElementById('birthday')
          .style
          .backgroundColor = 'transparent'
      }
    } else {
      swal('Data Inválida.')
      document
        .getElementById('birthday')
        .className += ' invalid'
      document
        .getElementById('birthday')
        .value = ''
    }
  })
}

// convert the date string in the format of dd/mm/yyyy into a JS date object
function parseDate (dateStr) {
  var dateParts = dateStr.split('/')
  return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0])
}

// is valid date format
function calculateAge (dateOfBirth, dateToCalculate) {
  var calculateYear = dateToCalculate.getFullYear()
  var calculateMonth = dateToCalculate.getMonth()
  var calculateDay = dateToCalculate.getDate()

  var birthYear = dateOfBirth.getFullYear()
  var birthMonth = dateOfBirth.getMonth()
  var birthDay = dateOfBirth.getDate()

  var age = calculateYear - birthYear
  var ageMonth = calculateMonth - birthMonth
  var ageDay = calculateDay - birthDay

  if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
    age = parseInt(age) - 1
  }
  return age
}

function isDate (txtDate) {
  var currVal = txtDate
  if (currVal == '') { return true }

  // Declare Regex
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/
  var dtArray = currVal.match(rxDatePattern) // is format OK?

  if (dtArray == null) { return false }

  // Checks for dd/mm/yyyy format.
  var dtDay = dtArray[1]
  var dtMonth = dtArray[3]
  var dtYear = dtArray[5]

  if (dtMonth < 1 || dtMonth > 12) { return false } else if (dtDay < 1 || dtDay > 31) { return false } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) { return false } else if (dtMonth == 2) {
    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0))
    if (dtDay > 29 || (dtDay == 29 && !isleap)) { return false }
  }

  return true
}

function fixStepIndicator (n) {
  // This function removes the "active" class of all steps...
  var i
  var x = document.getElementsByClassName('step')
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i]
      .className
      .replace(' active', '')
  }
  // ... and adds the "active" class on the current step:
  x[n].className += ' active'
}

document
  .getElementById('ocupation')
  .onchange = function () {
    this.style.backgroundColor = 'transparent'

    var ocupation = document.getElementById('ocupation')
    var ocupation_id = ocupation
      .options[ocupation.selectedIndex]
      .value

    fetch(`/candidate/degree/${ocupation_id}`).then(function (response) {
      response
        .text()
        .then(function (html) {
          document
            .getElementById('degree')
            .innerHTML = html
        })
    })
  }
