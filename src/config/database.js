module.exports = {
  username: 'senac',
  password: 'nTg10x!0aI',
  // username: 'docker',
  // password: 'docker',
  database: 'senacprocsel',
  //host: '192.168.0.35',
  //host: '200.223.180.163',
  //host: '177.47.185.137',
  // host: '127.0.0.1',
  port: '54322',
  dialect: 'postgres',
  operatorAliases: false,
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true
  }
}
